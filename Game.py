from Player import Player
from Player import Dealer
from Deck import Deck, Hand


class Game:
    """Клас для представлення гри в блекджек."""
    def __init__(self, player_name):
        """
        Ініціалізує гру з гравцем з вказаним іменем.

        Parameters:
            player_name (str): Ім'я гравця.
        """
        self.player = Player(player_name)
        self.dealer = Dealer()
        self.deck = Deck()
        self.bet = 0
        self.continue_playing = True

    def place_bet(self):
        """Дозволяє гравцю встановити ставку."""
        while True:
            try:
                self.bet = int(input("Place your bet: "))
                if self.bet > 0 and self.bet <= self.player.balance:
                    break
                else:
                    print(
                        f"Bet should be greater than 0 and less than or equal to your balance ({self.player.balance}).")
            except ValueError:
                print("Invalid input! Please enter a valid bet.")

    def deal_initial_cards(self):
        """Роздає початкові карти гравцю і дилеру."""
        self.player.hand = Hand()
        self.dealer.hand = Hand()
        self.player.hand.add_card(self.deck.draw_card())
        self.player.hand.add_card(self.deck.draw_card())
        self.dealer.hand.add_card(self.deck.draw_card())
        self.dealer.hand.add_card(self.deck.draw_card())

    def hit(self, participant):
        """
        Додає карту до руки заданого учасника.

        Parameters:
            participant (Player or Dealer): Учасник гри (гравець або дилер), якому потрібно додати карту.
        """
        participant.hand.add_card(self.deck.draw_card())
        participant.hand.adjust_for_ace()

    def dealer_play(self):
        """Автоматично грає за дилера."""
        while self.dealer.hand.value < 17:
            self.hit(self.dealer)

    def check_win_condition(self):
        """
        Перевіряє умови перемоги та змінює баланси гравця та дилера.

        Returns:
            str: Рядок, що містить результат гри.
        """
        player_score = self.player.hand.value
        dealer_score = self.dealer.hand.value

        if player_score > 21:
            self.player.adjust_balance(-self.bet)
            self.dealer.adjust_balance(self.bet)
            return "You busted! Dealer wins."

        elif dealer_score > 21:
            self.player.adjust_balance(self.bet)
            self.dealer.adjust_balance(-self.bet)
            return "Dealer busted! You win."

        elif player_score == dealer_score:
            return f"It's a tie! Your bet ({self.bet}) is returned."

        elif player_score == 21 or dealer_score > 21:
            self.player.adjust_balance(self.bet * 2)
            self.dealer.adjust_balance(-self.bet * 2)
            return f"Blackjack! You win {self.bet * 2}!"

        elif dealer_score == 21 or player_score > 21:
            self.player.adjust_balance(-self.bet)
            self.dealer.adjust_balance(self.bet)
            return f"Dealer got Blackjack! You lose {self.bet}."

        elif player_score > dealer_score:
            self.player.adjust_balance(self.bet * 2)
            self.dealer.adjust_balance(-self.bet * 2)
            return f"You win {self.bet * 2}!"

        else:
            self.player.adjust_balance(-self.bet)
            self.dealer.adjust_balance(self.bet)
            return f"You lose {self.bet}."

    def play(self):
        """Проводить розіграш, керує взаємодією з гравцями та дилером."""
        while self.continue_playing:
            self.place_bet()
            self.deal_initial_cards()
            print("Your cards:")
            print(self.player)
            print("Dealer's cards:")
            print(self.dealer)

            while True:
                action = input("Do you want to hit or stand? (h/s): ").lower()
                if action == 'h':
                    self.hit(self.player)
                    print("Your cards:")
                    print(self.player)
                    if self.player.hand.value > 21:
                        break
                elif action == 's':
                    break
                else:
                    print("Invalid input! Please enter 'h' to hit or 's' to stand.")

            self.dealer_play()
            print("Dealer's cards:")
            print(self.dealer)

            print(self.check_win_condition())
            print("Your current balance:", self.player.balance)
            print("Dealer's balance:", self.dealer.balance)

            while True:
                choice = input("Do you want to continue playing? (y/n): ").lower()
                if choice in ['y', 'n']:
                    break
                else:
                    print("Invalid input! Please enter 'y' to continue playing or 'n' to quit.")
            if choice == 'n':
                self.continue_playing = False
                print("Goodbye!", "Your current balance:", self.player.balance)
