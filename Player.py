from Deck import Hand


class Player:
    """Клас для представлення гравця."""
    def __init__(self, name, balance=100):
        """
        Ініціалізує гравця з іменем та балансом.

        Parameters:
            name (str): Ім'я гравця.
            balance (int): Початковий баланс гравця (за замовчуванням - 100).
        """
        self.name = name
        self.hand = Hand()
        self.balance = balance

    def __str__(self):
        """Повертає рядок, що містить ім'я гравця, їхні карти та значення руки, а також баланс."""
        return (f"{self.name}: {', '.join(str(card) for card in self.hand.cards)} (Value: {self.hand.value}), "
                f"Balance: {self.balance}")

    def adjust_balance(self, amount):
        """
        Змінює баланс гравця на задану суму.

        Parameters:
            amount (int): Сума, на яку потрібно змінити баланс гравця.
        """
        self.balance += amount


class Dealer(Player):
    """Клас для представлення дилера, який є спеціалізацією класу гравця."""
    def __init__(self, name="Dealer", balance=1000):
        """
        Ініціалізує дилера з ім'ям "Дилер" та балансом.

        Parameters:
            name (str): Ім'я дилера (за замовчуванням - "Дилер").
            balance (int): Початковий баланс дилера (за замовчуванням - 1000).
        """
        super().__init__(name)
        self.balance = balance

    def adjust_balance(self, amount):
        """
        Змінює баланс дилера на задану суму.

        Parameters:
            amount (int): Сума, на яку потрібно змінити баланс дилера.
        """
        self.balance += amount
