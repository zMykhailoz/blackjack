import unittest
from unittest.mock import patch
from Game import Game
from Deck import Card


class TestBlackjack(unittest.TestCase):
    """Клас для тестування функцій гри в блекджек."""
    @patch('builtins.input', side_effect=['50'])
    def test_place_bet(self, mock_input):
        """
        Тестує функцію place_bet().

        Перевіряє, чи правильно встановлюється ставка гравця.

        """
        game = Game("TestPlayer")
        game.place_bet()
        self.assertEqual(game.bet, 50)

    def test_deal_initial_cards(self):
        """
        Тестує функцію deal_initial_cards().

        Перевіряє, чи правильно роздаються початкові карти гравцеві та дилеру.

        """
        game = Game("TestPlayer")
        game.deal_initial_cards()
        self.assertEqual(len(game.player.hand.cards), 2)
        self.assertEqual(len(game.dealer.hand.cards), 2)

    def test_check_win_condition(self):
        """
        Тестує функцію check_win_condition().

        Перевіряє, чи правильно визначається результат гри при різних умовах.

        """
        game = Game("TestPlayer")
        game.player.hand.cards = [Card("Hearts", "9"), Card("Spades", "7")]
        game.dealer.hand.cards = [Card("Clubs", "9"), Card("Hearts", "7")]
        game.bet = 50
        result = game.check_win_condition()
        self.assertIn("It's a tie", result)  # Випадок нічії
        self.assertEqual(game.player.balance, 100)  # Баланс гравця не змінюється
        self.assertEqual(game.dealer.balance, 1000)  # Баланс дилера не змінюється


if __name__ == '__main__':
    unittest.main()
