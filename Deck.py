import random


class Card:
    """Клас для представлення карти гральної колоди."""
    def __init__(self, suit, rank):
        """
        Ініціалізує об'єкт карти з вказаними мастю та рангом.
            Parameters:
        suit (str): Масть карти (наприклад, "Hearts", "Diamonds", "Clubs", "Spades").
            rank (str): Ранг карти (наприклад, "2", "3", ..., "Ace").
        """
        self.suit = suit
        self.rank = rank

    def __str__(self):
        """Повертає рядок, який представляє карту у зручному для читання форматі."""
        return f"{self.rank} of {self.suit}"


class Deck:
    """Клас для представлення гральної колоди."""
    def __init__(self):
        """Ініціалізує гральну колоду і перемішує її."""
        suits = ['Hearts', 'Diamonds', 'Clubs', 'Spades']
        ranks = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'Jack', 'Queen', 'King', 'Ace']
        self.cards = [Card(suit, rank) for suit in suits for rank in ranks]
        random.shuffle(self.cards)

    def draw_card(self):
        """Видаляє та повертає верхню карту з гральної колоди."""
        return self.cards.pop()


class Hand:
    """Клас для представлення руки гравця або дилера."""
    def __init__(self):
        """Ініціалізує руку без карт та з нульовим значенням."""
        self.cards = []
        self.value = 0

    def add_card(self, card):
        """
        Додає карту до руки та оновлює значення руки.

        Parameters:
            card (Card): Карта, яка буде додана до руки.
        """
        self.cards.append(card)
        self.value += self.card_value(card)

    def card_value(self, card):
        """
        Визначає числове значення карти.

        Parameters:
            card (Card): Карта, для якої потрібно визначити значення.

        Returns:
            int: Числове значення карти.
        """
        if card.rank in ['Jack', 'Queen', 'King']:
            return 10
        elif card.rank == 'Ace':
            return 11 if self.value + 11 <= 21 else 1
        else:
            return int(card.rank)

    def adjust_for_ace(self):
        """При необхідності зменшує значення "ейса" з 11 до 1."""
        while self.value > 21 and self.cards.count('Ace'):
            self.value -= 10
            self.cards.remove('Ace')

